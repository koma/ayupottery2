#!/bin/bash

(( i = 0 ))
compress_new_images()
{
	output=`echo "$1" | sed "s/\.JPG/-cjpeg70.JPG/"`
	cjpeg -quality 70 -outfile "$output" "$1"
	echo "$output created"
	(( i++ ))
}

find . -name "*.JPG" | \
	(while read file; do compress_new_images "$file"; done \
	&& echo "$i images compressed")
