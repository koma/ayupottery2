#include "create-galerie.h"

std::vector<char *> get_filenames(char *path)
{
	DIR					*dirp;
	struct dirent		*dp;
	std::vector<char *>	jpegfiles;

	dirp = opendir(path);
	if (dirp != NULL)
	{
		errno = 0;
		dp = readdir(dirp);
		while (dp != NULL)
		{
			if (strcmp(dp->d_name + strlen(dp->d_name) - 4, ".jpg") == 0
					|| strcmp(dp->d_name + strlen(dp->d_name) - 4, ".JPG") == 0
					|| strcmp(dp->d_name + strlen(dp->d_name) - 5, ".jpeg") == 0
					|| strcmp(dp->d_name + strlen(dp->d_name) - 5, ".JPEG") == 0)
				jpegfiles.push_back(dp->d_name);
			dp = readdir(dirp);
		}
		if (dp == NULL && errno != 0)
			perror("readdir");
		closedir(dirp);
	}
	else
		perror("opendir");
	return (jpegfiles);
}

int		main(int argc, char **argv)
{
	//std::string	str;
	std::vector<char *>	jpegfiles;

	if (argc != 2)
	{
		std::cerr << "Takes one argument: the path to the directory containing the pictures" << std::endl;
		return (EXIT_FAILURE);
	}
	jpegfiles = get_filenames(argv[1]);
	if (jpegfiles.size() == 0)
		return (EXIT_FAILURE);
	write(STDOUT_FILENO, html_file_start, strlen(html_file_start));
	for (size_t i = 0; i < jpegfiles.size(); i++)
	{
		write(STDOUT_FILENO, html_slide_start, strlen(html_slide_start));
		write(STDOUT_FILENO, argv[1], strlen(argv[1]));
		write(STDOUT_FILENO, jpegfiles[i], strlen(jpegfiles[i]));
		write(STDOUT_FILENO, html_slide_end, strlen(html_slide_end));
	}
	write(STDOUT_FILENO, html_file_end, strlen(html_file_end));
	return (EXIT_SUCCESS);
}
