#ifndef CREATE_GALERIE_H
# define CREATE_GALERIE_H

# include <iostream>
# include <string.h>
# include <vector>
# include <errno.h>
# include <dirent.h>
# include <unistd.h>

const char	*html_file_start = "\
<!DOCTYPE html>\n\
\n\
<html>\n\
	<head>\n\
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\
		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\n\
		<script src=\"slideshow.js\" async></script>\n\
		<title>yakimono</title>\n\
		<meta charset=\"utf-8\">\n\
	</head>\n\
	<body>\n\
\n\
		<div class=\"slideshow-container\">\n\
\n\
";

const char	*html_slide_start = "\
			<figure class=\"one-slide\">\n\
				<img class=\"slide-pic\" src=\"\
";

const char	*html_slide_end = "\
\">\n\
				<figcaption class=\"slide-caption\"></figcaption>\n\
			</figure>\n\
\n\
";

const char	*html_file_end = "\
			<a class=\"prev-button\" onclick=\"nextSlide(-1)\">&#10094;</a>\n\
			<a class=\"next-button\" onclick=\"nextSlide(1)\">&#10095;</a>\n\
\n\
		</div>\n\
\n\
	</body>\n\
</html>\n\
";

#endif // CREATE_GALERIE_H
